require 'test_helper'

class UserBackoffice::WelcomeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get user_backoffice_welcome_index_url
    assert_response :success
  end

end
